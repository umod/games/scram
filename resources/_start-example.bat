@echo off
cls
:start
echo Starting server...

Windows_Core.bin -batchmode -nographics -logfile output_log.txt -name "My uMod Server" -map "Evacuation.Barn" -roomPort 1337 -queryPort 27015 -players 30

echo.
echo Restarting server...
timeout /t 10
echo.
goto start
