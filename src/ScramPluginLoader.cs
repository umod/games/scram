﻿using System;
using uMod.Plugins;

namespace uMod.Scram
{
    /// <summary>
    /// Responsible for loading the core Scram plugin
    /// </summary>
    public class ScramPluginLoader : PluginLoader
    {
        public override Type[] CorePlugins => new[] { typeof(Scram) };
    }
}
