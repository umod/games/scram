﻿using Bolt;
using Scram;
using System;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using uMod.Libraries;
using uMod.Libraries.Universal;
using UnityEngine;

namespace uMod.Scram
{
    /// <summary>
    /// Represents a player, either connected or not
    /// </summary>
    public class ScramPlayer : IPlayer, IEquatable<IPlayer>
    {
        private static Permission permission;
        private readonly BoltConnection boltConnection;
        private readonly PlayerConnection connection;
        private readonly ulong steamId;

        internal ScramPlayer(string playerId, string playerName)
        {
            if (permission == null)
            {
                permission = Interface.uMod.GetLibrary<Permission>();
            }

            Id = playerId;
            Name = Regex.Replace(playerName, @"</?noparse>", "").Sanitize();
            steamId = ulong.Parse(playerId);
        }

        internal ScramPlayer(PlayerConnection connection) : this(connection.SteamID.ToString(), connection.PlayerInfo.state.PenName)
        {
            this.boltConnection = connection.BoltConnection;
            this.connection = connection;
        }

        #region Objects

        /// <summary>
        /// Gets the object that backs the player
        /// </summary>
        public object Object => connection;

        /// <summary>
        /// Gets the player's last command type
        /// </summary>
        public CommandType LastCommand { get; set; }

        #endregion Objects

        #region Information

        /// <summary>
        /// Gets/sets the name for the player
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets the ID for the player (unique within the current game)
        /// </summary>
        public string Id { get; }

        /// <summary>
        /// Gets the player's language
        /// </summary>
        public CultureInfo Language => CultureInfo.GetCultureInfo("en"); // TODO: Implement when possible

        /// <summary>
        /// Gets the player's IP address
        /// </summary>
        public string Address => boltConnection.RemoteEndPoint.Address.ToString() ?? "0.0.0.0"; // TODO: Test this

        /// <summary>
        /// Gets the player's average network ping
        /// </summary>
        public int Ping => Convert.ToInt32(boltConnection.PingNetwork);

        /// <summary>
        /// Gets if the player is a server admin
        /// </summary>
        public bool IsAdmin => false; // TODO: Implement when possible

        /// <summary>
        /// Gets if the player is a server moderator
        /// </summary>
        public bool IsModerator => IsAdmin;

        /// <summary>
        /// Gets if the player is banned
        /// </summary>
        public bool IsBanned => false; // TODO: Implement when possible

        /// <summary>
        /// Gets if the player is connected
        /// </summary>
        public bool IsConnected => HostPlayerRegistry.Instance.PlayerConnections.Contains(connection); // TODO: This or HostNetwork.spleens?

        /// <summary>
        /// Gets if the player is alive
        /// </summary>
        public bool IsAlive => connection.Player.entity.isAttached && connection.Player.entity.isControlled;

        /// <summary>
        /// Gets if the player is dead
        /// </summary>
        public bool IsDead => connection.Player == null;

        /// <summary>
        /// Gets if the player is sleeping
        /// </summary>
        public bool IsSleeping
        {
            get
            {
                return false; // TODO: Implement when possible
            }
        }

        /// <summary>
        /// Gets if the player is the server
        /// </summary>
        public bool IsServer => false;

        #endregion Information

        #region Administration

        /// <summary>
        /// Bans the player for the specified reason and duration
        /// </summary>
        /// <param name="reason"></param>
        /// <param name="duration"></param>
        public void Ban(string reason, TimeSpan duration = default(TimeSpan))
        {
            // Check if already banned
            if (!IsBanned)
            {
                // Ban and kick user
                // TODO: Implement when possible
            }
        }

        /// <summary>
        /// Gets the amount of time remaining on the player's ban
        /// </summary>
        public TimeSpan BanTimeRemaining
        {
            get
            {
                return DateTime.MaxValue.TimeOfDay; // TODO: Implement when possible
            }
        }

        /// <summary>
        /// Heals the player's character by specified amount
        /// </summary>
        /// <param name="amount"></param>
        public void Heal(float amount)
        {
            connection.Player.state.BriefHealth = 0; // TODO: Unsure if this is necessary/relevant
            connection.Player.state.ActiveHealth = Mathf.Max((int)amount + connection.Player.state.ActiveHealth, connection.Player.state.MaxHealth);
        }

        /// <summary>
        /// Gets/sets the player's health
        /// </summary>
        public float Health
        {
            get => connection.Player.state.ActiveHealth;
            set => connection.Player.state.ActiveHealth = (int)value;
        }

        /// <summary>
        /// Damages the player's character by specified amount
        /// </summary>
        /// <param name="amount"></param>
        public void Hurt(float amount)
        {
            connection.Player.state.ActiveHealth -= (int)amount;
        }

        /// <summary>
        /// Kicks the player from the game
        /// </summary>
        /// <param name="reason"></param>
        public void Kick(string reason)
        {
            boltConnection.Disconnect(new RefuseToken()
            {
                RefuseReason = reason
            });
        }

        /// <summary>
        /// Causes the player's character to die
        /// </summary>
        public void Kill() => connection.Player.Die(Vector3.up);

        /// <summary>
        /// Gets/sets the player's maximum health
        /// </summary>
        public float MaxHealth
        {
            get => connection.Player.state.MaxHealth;
            set => connection.Player.state.MaxHealth = (int)value;
        }

        /// <summary>
        /// Renames the player to specified name
        /// </summary>
        /// <param name="name"></param>
        public void Rename(string name)
        {
            name = string.IsNullOrEmpty(name.Trim()) ? connection.Player.state.PenName : name.Sanitize(); // TODO: Wrap new name with <noparse></noparse> to match game
            PlayerConnection registredConnection = HostPlayerRegistry.Instance.PlayerConnections.First(p => p.SteamID == connection.SteamID); // TODO: Check if this is necessary
            registredConnection.PlayerInfo.state.PenName = name; // TODO: Check if this is necessary
            registredConnection.Player.state.PenName = name; // TODO: Check if this is necessary
            connection.PlayerInfo.state.PenName = name; // TODO: Check if this is necessary
            connection.Player.state.PenName = name; // TODO: Check if this is necessary
            connection.IPlayer.Name = name;
            permission.UpdateNickname(steamId.ToString(), name);
        }

        /// <summary>
        /// Teleports the player's character to the specified position
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="z"></param>
        public void Teleport(float x, float y, float z)
        {
            connection.Player.entity.transform.position = new Vector3(x, y, z);
        }

        /// <summary>
        /// Teleports the player's character to the specified generic position
        /// </summary>
        /// <param name="pos"></param>
        public void Teleport(GenericPosition position) => Teleport(position.X, position.Y, position.Z);

        /// <summary>
        /// Unbans the player
        /// </summary>
        public void Unban()
        {
            // Check if unbanned already
            if (IsBanned)
            {
                // Set to unbanned
                // TODO: Implement when possible
            }
        }

        #endregion Administration

        #region Location

        /// <summary>
        /// Gets the position of the player
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="z"></param>
        public void Position(out float x, out float y, out float z)
        {
            GenericPosition position = Position();
            x = position.X;
            y = position.Y;
            z = position.Z;
        }

        /// <summary>
        /// Gets the position of the player
        /// </summary>
        /// <returns></returns>
        public GenericPosition Position()
        {
            Vector3 position = connection.Player.entity.transform.position;
            return new GenericPosition(position.x, position.y, position.z);
        }

        #endregion Location

        #region Chat and Commands

        /// <summary>
        /// Sends the specified message and prefix to the player
        /// </summary>
        /// <param name="message"></param>
        /// <param name="prefix"></param>
        /// <param name="args"></param>
        public void Message(string message, string prefix, params object[] args)
        {
            if (!string.IsNullOrEmpty(message))
            {
                ulong avatarId = args.Length > 0 && args[0].IsSteamId() ? (ulong)args[0] : 0ul;
                message = args.Length > 0 ? string.Format(Formatter.ToUnity(message), avatarId != 0ul ? args.Skip(1) : args) : Formatter.ToUnity(message);
                BoltGlobalEvent.SendPrivateMessage(prefix != null ? $"{prefix} {message}" : message, Color.white, connection.BoltConnection);
            }
        }

        /// <summary>
        /// Sends the specified message to the player
        /// </summary>
        /// <param name="message"></param>
        public void Message(string message) => Message(message, null);

        /// <summary>
        /// Replies to the player with the specified message and prefix
        /// </summary>
        /// <param name="message"></param>
        /// <param name="prefix"></param>
        /// <param name="args"></param>
        public void Reply(string message, string prefix, params object[] args) => Message(message, prefix, args);

        /// <summary>
        /// Replies to the player with the specified message
        /// </summary>
        /// <param name="message"></param>
        public void Reply(string message) => Message(message, null);

        /// <summary>
        /// Runs the specified console command on the player
        /// </summary>
        /// <param name="command"></param>
        /// <param name="args"></param>
        public void Command(string command, params object[] args)
        {
            // TODO: Implement when possible
        }

        #endregion Chat and Commands

        #region Permissions

        /// <summary>
        /// Gets if the player has the specified permission
        /// </summary>
        /// <param name="perm"></param>
        /// <returns></returns>
        public bool HasPermission(string perm) => permission.UserHasPermission(Id, perm);

        /// <summary>
        /// Grants the specified permission on this player
        /// </summary>
        /// <param name="perm"></param>
        public void GrantPermission(string perm) => permission.GrantUserPermission(Id, perm, null);

        /// <summary>
        /// Strips the specified permission from this player
        /// </summary>
        /// <param name="perm"></param>
        public void RevokePermission(string perm) => permission.RevokeUserPermission(Id, perm);

        /// <summary>
        /// Gets if the player belongs to the specified group
        /// </summary>
        /// <param name="group"></param>
        /// <returns></returns>
        public bool BelongsToGroup(string group) => permission.UserHasGroup(Id, group);

        /// <summary>
        /// Adds the player to the specified group
        /// </summary>
        /// <param name="group"></param>
        public void AddToGroup(string group) => permission.AddUserGroup(Id, group);

        /// <summary>
        /// Removes the player from the specified group
        /// </summary>
        /// <param name="group"></param>
        public void RemoveFromGroup(string group) => permission.RemoveUserGroup(Id, group);

        #endregion Permissions

        #region Operator Overloads

        /// <summary>
        /// Gets if player's unique ID is equal to another player's unique ID
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public bool Equals(IPlayer other) => Id == other?.Id;

        /// <summary>
        /// Gets if player's object is equal to another player's object
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object obj) => obj is IPlayer && Id == ((IPlayer)obj).Id;

        /// <summary>
        /// Gets the hash code of the player's unique ID
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode() => Id.GetHashCode();

        /// <summary>
        /// Returns a human readable string representation of this IPlayer
        /// </summary>
        /// <returns></returns>
        public override string ToString() => $"ScramPlayer[{Id}, {Name}]";

        #endregion Operator Overloads
    }
}
