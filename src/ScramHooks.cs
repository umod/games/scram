using Scram;
using System.Linq;
using System.Text.RegularExpressions;
using UdpKit;
using uMod.Configuration;
using uMod.Libraries.Universal;
using uMod.Plugins;
using UnityEngine;

namespace uMod.Scram
{
    /// <summary>
    /// Game hooks and wrappers for the core Scram plugin
    /// </summary>
    public partial class Scram
    {
        #region Player Hooks

        /// <summary>
        /// Called when the player is attempting to connect
        /// </summary>
        /// <param name="connection"></param>
        /// <returns></returns>
        [HookMethod("IOnPlayerConnect")]
        private object IOnPlayerConnect(UdpEndPoint endpoint, ConnectToken token)
        {
            // Let universal know player is joining
            Universal.PlayerManager.PlayerJoin(token.SteamID.ToString(), token.PenName); // TODO: Handle this automatically

            // Call universal hook
            object canLogin = Interface.Call("CanPlayerConnect", token.PenName, token.SteamID.ToString(), endpoint.Address.ToString());
            if (canLogin is string || canLogin is bool && !(bool)canLogin)
            {
                BoltNetwork.Refuse(endpoint, new RefuseToken
                {
                    RefuseReason = canLogin is string ? canLogin.ToString() : "Connection was rejected" // TODO: Localization
                });
                return true;
            }

            // Let plugins know
            Interface.Call("OnPlayerApproved", token.PenName, token.SteamID.ToString(), endpoint.Address.ToString());

            return null;
        }

        /// <summary>
        /// Called when the player sends a chat message
        /// </summary>
        /// <param name="boltConnection"></param>
        /// <returns></returns>
        [HookMethod("IOnPlayerChat")]
        private object IOnPlayerChat(BoltConnection boltConnection, string message)
        {
            PlayerConnection connection = boltConnection.GetPlayerConnection();
            message = message.Trim();

            // Ignore empty/short messages
            if (string.IsNullOrEmpty(message) || message.Length <= 1)
            {
                return true;
            }

            // Check if it is a chat command
            if (message[0] != Interface.uMod.Config.Options.ChatCommandPrefix && message[0] != '!')
            {
                // Call game-specific and universal hooks
                object hookSpecific = Interface.Call("OnPlayerChat", connection, message);
                object hookUniversal = Interface.Call("OnPlayerChat", connection.IPlayer, message);
                if (hookSpecific != null || hookUniversal != null)
                {
                    // Block chat message
                    return true;
                }

                // Allow chat message
                return null;
            }

            // Extract command and args
            string[] commandString = message.Split(' ');
            string command = commandString[0].Substring(1);
            string[] args = commandString.Skip(1).ToArray();

            // Get universal player object
            IPlayer player = connection.IPlayer;
            if (player == null)
            {
                return null;
            }

            // Is the command blocked?
            object commandSpecific = Interface.CallHook("OnPlayerCommand", connection, command, args);
            object commandUniversal = Interface.CallHook("OnPlayerCommand", player, command, args);
            if (commandSpecific != null || commandUniversal != null)
            {
                return true;
            }

            // Is it a valid chat command?
            if (!Universal.CommandSystem.HandleChatMessage(player, message))
            {
                player.Reply(string.Format(lang.GetMessage("UnknownCommand", this, player.Id), command));
            }

            return true;
        }

        /// <summary>
        /// Called when the player has connected
        /// </summary>
        /// <param name="boltConnection"></param>
        /// <returns></returns>
        [HookMethod("IOnPlayerConnected")]
        private void IOnPlayerConnected(PlayerConnection connection)
        {
            string steamId = connection.SteamID.ToString();

            if (permission.IsLoaded)
            {
                // Update player's stored username
                permission.UpdateNickname(steamId, Regex.Replace(connection.PlayerInfo.state.PenName, @"</?noparse>", ""));

                // Set default groups, if necessary
                uModConfig.DefaultGroups defaultGroups = Interface.uMod.Config.Options.DefaultGroups;
                if (!permission.UserHasGroup(steamId, defaultGroups.Players))
                {
                    permission.AddUserGroup(steamId, defaultGroups.Players);
                }
            }

            // Let universal know player connected
            Universal.PlayerManager.PlayerConnected(connection);

            // Call game-specific hook
            Interface.Call("OnPlayerConnected", connection);

            // Find universal player
            IPlayer player = Universal.PlayerManager.FindPlayerById(steamId);
            if (player != null)
            {
                // Set IPlayer object on Player
                connection.IPlayer = player;

                // Call universal hook
                Interface.Call("OnPlayerConnected", player);
            }
        }

        /// <summary>
        /// Called when the player has disconnected
        /// </summary>
        /// <param name="boltConnection"></param>
        [HookMethod("IOnPlayerDisconnected")]
        private void IOnPlayerDisconnected(BoltConnection boltConnection)
        {
            PlayerConnection connection = boltConnection.GetPlayerConnection();

            // Call game-specific hook
            Interface.Call("OnPlayerDisconnected", connection);

            // Call universal hook
            Interface.Call("OnPlayerDisconnected", connection.IPlayer, lang.GetMessage("Unknown", this, connection.IPlayer.Id));

            // Let universal know
            Universal.PlayerManager.PlayerDisconnected(connection);
        }

        /// <summary>
        /// Called when the player has spawned
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="position"></param>
        [HookMethod("OnPlayerSpawned")]
        private void OnPlayerSpawned(PlayerConnection connection, Vector3 position)
        {
            // Call universal hook
            Interface.Call("OnPlayerSpawned", connection.IPlayer, new GenericPosition(position.x, position.y, position.z)); // TODO: Use new overload when available
        }

        #endregion Player Hooks
    }
}
